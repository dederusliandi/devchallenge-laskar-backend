const express = require("express");
const app = express();

// initialize route
const projectRoutes = require("./api/routes/projects");

// Routes which should handle requests
app.use("/api/projects", projectRoutes);

app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;