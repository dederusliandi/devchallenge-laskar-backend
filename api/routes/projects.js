const express = require('express');
const router = express.Router();

// include request
var request = require('request');

const userAuth = {
    username: 'devchallenge',
    password: 'dev12345'
};

router.get('/', (req, res, next) => {
    var options = {
        method: 'GET',  
        qs: {expand: 'description,lead'},      
        url: 'http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project',
        auth: userAuth,
        headers: {
           'Accept': 'application/json'
        }
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        var obj = JSON.parse(body);
        var result = [];

        obj.forEach(function(table) {
            var data = {
                'id': table.id,
                'name': table.name,
                'description': table.description,
                'stakeholder': table.lead.name,
                'unit': table.projectTypeKey
            };
            result.push(data);
        });

        var data = {
            'status': 200,
            'message': 'list of all project',
            'data': result
        };

        res.send(data);
    });
});

router.get('/:typeKey', function(req, res, next) {
    var options = {
        method: 'GET',
        qs: {expand: 'description,lead,projectKey'},     
        url: 'http://jira-telkomdds-devops-playground.apps.playcourt.id/rest/api/2/project/' + req.params.typeKey,
        auth: userAuth,
        headers: {
           'Accept': 'application/json'
        }
    };
     
    request(options, function (error, response, body) {
        if (error) throw new Error(error);
        var obj = JSON.parse(body);
    
        var result = {
            'id': obj.id,
            'name': obj.name,
            'description': obj.description,
            'stakeholder': obj.lead.name,
            'unit': obj.projectTypeKey
        };

        var data = {
            'status': 200,
            'message': 'list of all project',
            'data': result
        };

        res.send(data);
    });
});

module.exports = router;